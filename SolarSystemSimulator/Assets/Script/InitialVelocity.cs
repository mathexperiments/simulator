﻿using UnityEngine;

using System;
using System.Collections;

namespace SolarSystem {

    [RequireComponent(typeof(Rigidbody))]
    public class InitialVelocity : MonoBehaviour {

        [SerializeField]
        private Vector3 velocity;
        public Vector3 Velocity {
            get { return this.velocity; }
        }

        private void Awake() {
            this.UpdateVelocity();
        }

        public void UpdateVelocity() {
            this.GetComponent<Rigidbody>().AddForce(this.velocity);
        }
    }

}