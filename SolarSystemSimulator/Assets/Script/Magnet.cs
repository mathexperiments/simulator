﻿using UnityEngine;

using System;
using System.Collections;

namespace SolarSystem {

    [Flags]
    public enum EPoles {
        ATTRACT,
        DEFLECT,
    };

    [RequireComponent(typeof(Rigidbody))]
    public class Magnet : MonoBehaviour {



        [SerializeField]
        private GameObject target;

        [SerializeField]
        private bool isStatic = false;
        public bool IsStatic {
            get { return this.isStatic; }
        }

        [SerializeField]
        private bool useGravity = false;
        public bool UseGravity {
            get { return this.useGravity; }
        }

        [SerializeField]
        private EPoles pole = EPoles.ATTRACT;
        public EPoles Pole {
            get { return this.pole; }
        }

        private void Awake() {
            this.GetComponent<Rigidbody>().isKinematic = this.isStatic;
            this.GetComponent<Rigidbody>().useGravity = this.useGravity;
        }

        private void FixedUpdate() {
            //this.GetComponent<Rigidbody>().AddRelativeForce (this.target.transform.position - this.transform.position);
            //this.GetComponent<Rigidbody>().AddForce (this.target.transform.position - this.transform.position);
            //this.GetComponent<Rigidbody>().AddRelativeForce (Target.GetGravity(this.transform.position));
            this.GetComponent<Rigidbody>().AddForce(Target.GetGravity(this.transform.position));
        }
    }

}